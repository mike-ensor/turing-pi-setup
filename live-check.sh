#!/bin/bash

PING_RUNS=2

function is_live() {
    # echo "called $1"
    local server=$1
    raw=$(ping -c ${PING_RUNS} "192.168.86.20${server}")
    result=$?
    is_error=0
    if [ "$result" -ne "0" ]; then
        # echo "Result is: $result --- from ${raw}"
        is_error=1
    fi
    # echo "Returning ${is_error}"
    echo "$is_error"
}

for N in 1 2 3 4 5 6 7
do
    server=$(is_live "${N}")
    OUT_STRING="Server 192.168.86.20${N}"
    if [ "${server}" == "1" ]; then
        echo "${OUT_STRING}: FAIL"
    else
        echo "${OUT_STRING}: OK"
    fi
done
# Overview

A series of scripts and notes to build and rebuild a Turing Pi

# IP Addresses

IP addresses were assigned with my router in a `192.168.xxx.xxN` format (example: 192.168.1.101 = 'black-pearl-1')

# SSH Keys

Create a new ssh key and copy the public key string to insert into `~/.ssh/authorized_keys` (alternative to using `cloud-init` process of updating the `user-data` file)

1. Create key pair: `ssh-keygen -t rsa -b 4096 -f ~/.ssh/turing-pi`
1. Copy key value: `cat ~/.ssh/turing-pi` (copy full string...if using mac, pipe to `pbcopy`)
1. Add SSH key config to associate `ip` with `user` and `key`:
    > Note: nearly my full `~/.ssh/config` file, removed IP addresses
    ```
    # Turing Pi Master
    Host 192.168.xxx.101
    HostName 192.168.xxx.101
    User pirate
    IdentityFile ~/.ssh/turing-pi

    # Turing Pi Worker 1
    Host 192.168.xxx.102
    HostName 192.168.xxx.102
    User pirate
    IdentityFile ~/.ssh/turing-pi

    # Turing Pi Worker 2
    Host 192.168.xxx.103
    HostName 192.168.xxx.103
    User pirate
    IdentityFile ~/.ssh/turing-pi

    # Turing Pi Worker 3
    Host 192.168.xxx.104
    HostName 192.168.xxx.104
    User pirate
    IdentityFile ~/.ssh/turing-pi

    # Turing Pi Worker 4
    Host 192.168.xxx.105
    HostName 192.168.xxx.105
    User pirate
    IdentityFile ~/.ssh/turing-pi

    # Turing Pi Worker 5
    Host 192.168.xxx.106
    HostName 192.168.xxx.106
    User pirate
    IdentityFile ~/.ssh/turing-pi

    # Turing Pi Worker 6
    Host 192.168.xxx.107
    HostName 192.168.xxx.107
    User pirate
    IdentityFile ~/.ssh/turing-pi
    ```
1. SSH into each box: `ssh pirate@192.168.xxx.10N` (accept signature by typing "yes")
    1. Make an SSH folder: `mkdir ~/.ssh`
    1. Add contents of ssh public key to authorized keys: `nano ~/.ssh/authorized_keys` (paste as 1 full line string, ie: no line breaks)
    1. Limit visibility: `chmod 400 ~/.ssh/authorized_keys`
    1. Exit & re-ssh back in (shouldn't need a password)

# Scripts

1. live-check.sh - Runs a Ping against the IPs assigned to my Turing Pi's RBP3+ Compute Modules

>NOTE: Assigned IPs were done with my router, not on the OS

# Install k3s

> Following [Jeff's scripts](https://www.jeffgeerling.com/blog/2020/installing-k3s-kubernetes-on-turing-pi-raspberry-pi-cluster-episode-3) pretty closely. The updated `playbook` has an "sample" folder now, so changed the install to:

`ansible-playbook -i site.yaml